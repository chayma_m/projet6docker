# Utilisez une image de base contenant Java et Tomcat
FROM tomcat:latest


# WORKDIR /usr/local/tomcat

# Copiez votre application WAR dans le répertoire webapps de Tomcat
COPY ./webapp/target/webapp.war /usr/local/tomcat/webapps/


# Exposez le port sur lequel Tomcat s'exécute (par défaut : 8080)
EXPOSE 8080
